FROM ubuntu:18.04

# Create an app directory
RUN mkdir -p /app

# copy the project folders into app directory
ADD . /app

WORKDIR /app
RUN chmod +x demo-cicd.sh

CMD ["/bin/bash", "/app/demo-cicd.sh", "Sean", "Mael"]

